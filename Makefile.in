VERSION=4.00

srcdir=@srcdir@
VPATH=@srcdir@

prefix=@prefix@
exec_prefix=@exec_prefix@
bindir=@bindir@
mandir=@mandir@
datadir=@datadir@
XSLTPROC = /usr/bin/xsltproc
INSTALLCMD=@INSTALL@
LIBS=@LIBS@ -lpopt -lz -lsmbclient

CC=@CC@
CFLAGS=@CFLAGS@ -I. -DVERSION=\"$(VERSION)\" -DDATADIR=\"$(datadir)\"
EXEEXT=@EXEEXT@

LIBNFS_OBJ = libnfs.o mount_client.o nfs_client.o mount_xdr.o nfs_xdr.o

DB_OBJS = fileio.o util.o dbench.o child.o system.o snprintf.o sockio.o nfsio.o libnfs.a socklib.o linux_scsi.o iscsi.o smb.o
SRV_OBJS = util.o tbench_srv.o socklib.o

all: dbench tbench nfsbench tbench_srv doc

dbench: $(DB_OBJS)
	$(CC) -o $@ $(DB_OBJS) $(LIBS)

tbench_srv: $(SRV_OBJS)
	$(CC) -o $@ $(SRV_OBJS) $(LIBS)

tbench: dbench
	ln -sf dbench tbench

nfsbench: dbench
	ln -sf dbench nfsbench

libnfs.a: $(LIBNFS_OBJ) 
	@echo Creating library $@
	ar r libnfs.a $(LIBNFS_OBJ) 
	ranlib libnfs.a

nfsio.o: nfsio.c mount.h nfs.h
	@echo Compiling $@
	gcc -g -c nfsio.c -o $@

libnfs.o: libnfs.c libnfs.h mount.h nfs.h
	@echo Compiling $@
	gcc -g -c libnfs.c -o $@

mount.h: mount.x
	@echo Generating $@
	rpcgen -h mount.x > mount.h

nfs.h: nfs.x
	@echo Generating $@
	rpcgen -h nfs.x > nfs.h

mount_xdr.o: mount_xdr.c mount.h
	@echo Compiling $@
	gcc -g -c mount_xdr.c -o $@

mount_xdr.c: mount.x
	@echo Generating $@
	rpcgen -c mount.x > mount_xdr.c

mount_client.o: mount_client.c mount.h
	@echo Compiling $@
	gcc -g -c mount_client.c -o $@

mount_client.c: mount.x
	@echo Generating $@
	rpcgen -l mount.x > mount_client.c

nfs_xdr.o: nfs_xdr.c nfs.h
	@echo Compiling $@
	gcc -g -c nfs_xdr.c -o $@

nfs_xdr.c: nfs.x
	@echo Generating $@
	rpcgen -c nfs.x > nfs_xdr.c

nfs_client.o: nfs_client.c nfs.h
	@echo Compiling $@
	gcc -g -c nfs_client.c -o $@

nfs_client.c: nfs.x
	@echo Generating $@
	rpcgen -l nfs.x > nfs_client.c

doc/dbench.1.html: doc/dbench.1.xml
	-test -z "$(XSLTPROC)" || $(XSLTPROC) -o $@ http://docbook.sourceforge.net/release/xsl/current/html/docbook.xsl $<

doc/dbench.1: doc/dbench.1.xml
	-test -z "$(XSLTPROC)" || $(XSLTPROC) -o $@ http://docbook.sourceforge.net/release/xsl/current/manpages/docbook.xsl $<

doc: doc/dbench.1 doc/dbench.1.html

# Careful here: don't install client.txt over itself.
install: all
	${INSTALLCMD} -d $(bindir) $(datadir) $(mandir)
	${INSTALLCMD} dbench tbench_srv $(bindir)
	${INSTALLCMD} loadfiles/client.txt $(datadir)
	${INSTALLCMD} loadfiles/nfs.txt $(datadir)
	${INSTALLCMD} -m644 dbench.1 $(mandir)
	ln -sf dbench.1 $(mandir)/tbench.1
	ln -sf dbench.1 $(mandir)/tbench_srv.1
	ln -sf $(bindir)/dbench $(bindir)/tbench 
	ln -sf $(bindir)/dbench $(bindir)/nfsbench 

clean:
	rm -f *.[ao] *~ dbench tbench_srv
	rm -f mount.h mount_xdr.c mount_client.c
	rm -f nfs.h nfs_xdr.c nfs_client.c 

proto:
	./mkproto.pl *.c > proto.h
